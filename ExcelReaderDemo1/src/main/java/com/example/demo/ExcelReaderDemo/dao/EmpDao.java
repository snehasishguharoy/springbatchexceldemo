package com.example.demo.ExcelReaderDemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.ExcelReaderDemo.model.Employee;

@Repository
public interface EmpDao extends JpaRepository<Employee,Long> {

}

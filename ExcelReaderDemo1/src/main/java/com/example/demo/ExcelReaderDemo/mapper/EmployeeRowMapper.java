package com.example.demo.ExcelReaderDemo.mapper;

import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.support.rowset.RowSet;

import com.example.demo.ExcelReaderDemo.model.Employee;

public class EmployeeRowMapper implements RowMapper<Employee> {

	@Override
	public Employee mapRow(RowSet rs) throws Exception {
		Employee employee=new Employee();
		employee.setEmpName(rs.getColumnValue(0));
		employee.setEmpDept(rs.getColumnValue(1));
		employee.setEmpAddr(rs.getColumnValue(2));
		employee.setEmpSal(rs.getColumnValue(3));
		employee.setEmpDesgn(rs.getColumnValue(4));
		employee.setEmpSex(rs.getColumnValue(5));
		return employee;
	}
}

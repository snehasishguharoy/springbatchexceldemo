package com.example.demo.ExcelReaderDemo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employee {
	@Id
	@Column(name="Emp_Name")
	private String empName;
	@Column(name="Emp_Dept")
	private String empDept;
	
	@Column(name="Emp_Addr")
	private String empAddr;
	@Column(name="Emp_Sal")
	private String empSal;
	@Column(name="Emp_Desgn")
	private String empDesgn;
	@Column(name="Emp_Sex")
	private String empSex;
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpDept() {
		return empDept;
	}
	public void setEmpDept(String empDept) {
		this.empDept = empDept;
	}
	public String getEmpAddr() {
		return empAddr;
	}
	public void setEmpAddr(String empAddr) {
		this.empAddr = empAddr;
	}
	public String getEmpSal() {
		return empSal;
	}
	public void setEmpSal(String empSal) {
		this.empSal = empSal;
	}
	public String getEmpDesgn() {
		return empDesgn;
	}
	public void setEmpDesgn(String empDesgn) {
		this.empDesgn = empDesgn;
	}
	@Override
	public String toString() {
		return "Employee [empName=" + empName + ", empDept=" + empDept + ", empAddr=" + empAddr + ", empSal=" + empSal
				+ ", empDesgn=" + empDesgn + "]";
	}
	public String getEmpSex() {
		return empSex;
	}
	public void setEmpSex(String empSex) {
		this.empSex = empSex;
	}



}

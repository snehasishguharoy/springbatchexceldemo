package com.example.demo.ExcelReaderDemo.prcssr;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.example.demo.ExcelReaderDemo.model.Employee;


@Component
public class EmployeeItemPrcssr implements ItemProcessor<Employee,Employee> {

	@Override
	public Employee process(Employee item) throws Exception {
		item.getEmpName().toUpperCase();
		return item;
	}

}

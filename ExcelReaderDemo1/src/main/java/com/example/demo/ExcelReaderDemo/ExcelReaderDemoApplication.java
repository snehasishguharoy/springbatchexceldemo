package com.example.demo.ExcelReaderDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class ExcelReaderDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExcelReaderDemoApplication.class, args);
	}

}


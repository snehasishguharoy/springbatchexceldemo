package com.example.demo.ExcelReaderDemo.wrtr;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.ExcelReaderDemo.dao.EmpDao;
import com.example.demo.ExcelReaderDemo.model.Employee;
@Component
public class EmployeeItemWrtr implements ItemWriter<Employee> {
	@Autowired
	private EmpDao empDao;
	@Override
	public void write(List<? extends Employee> items) throws Exception {
		for (Employee employee : items) {
			System.out.println(employee);
			empDao.save(employee);
		}

	}

}

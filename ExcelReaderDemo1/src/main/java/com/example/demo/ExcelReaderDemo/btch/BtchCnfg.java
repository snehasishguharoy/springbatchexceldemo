package com.example.demo.ExcelReaderDemo.btch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.poi.PoiItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.example.demo.ExcelReaderDemo.lstnr.BatchJobCompletionListener;
import com.example.demo.ExcelReaderDemo.mapper.EmployeeRowMapper;
import com.example.demo.ExcelReaderDemo.model.Employee;
import com.example.demo.ExcelReaderDemo.prcssr.EmployeeItemPrcssr;
import com.example.demo.ExcelReaderDemo.wrtr.EmployeeItemWrtr;



@Configuration
@EnableBatchProcessing
public class BtchCnfg {

	@Autowired
	private JobBuilderFactory builderFactory;
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
//	@Autowired
//	private DataSource dataSource;
	@Autowired
	private EmployeeItemPrcssr prcssr;
	@Autowired
	private EmployeeItemWrtr employeeItmwrtr;

	@Bean(destroyMethod = "")
	public Job exportEmployeeJob() {
		return builderFactory.get("exportEmployeeJob").incrementer(new RunIdIncrementer()).listener(listener())
				.flow(step1()).end().build();
	}

	@Bean(destroyMethod = "")
	public JobExecutionListener listener() {
		return new BatchJobCompletionListener();
	}

	@Bean(destroyMethod = "")
	public Step step1() {
		return stepBuilderFactory.get("step1").<Employee, Employee>chunk(100).reader(itemReader()).processor(prcssr)
				.writer(employeeItmwrtr).build();
	}

	@Bean(destroyMethod = "")
	public ItemReader<Employee> itemReader() {
		PoiItemReader<Employee> itemReader = new PoiItemReader<>();
		itemReader.setResource(new ClassPathResource("Employee.xlsx"));
		itemReader.setRowMapper(excelRowMapper());
		itemReader.setLinesToSkip(1);
		return itemReader;
	}

	private RowMapper<Employee> excelRowMapper() {
		return new EmployeeRowMapper();
	}

}
